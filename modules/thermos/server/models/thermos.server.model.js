'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Thermos Schema
 */
var ThermosSchema = new Schema({
  deviceId: {
    type: String,
    default: '',
    trim: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  epoch: {
    type: Number,
    default: 0
  },
  humidity: {
    type: Number,
    default: 0
  },
  celsius: {
    type: Number,
    default: 0
  },
  fahrenheit: {
    type: Number,
    default: 0
  }
});

mongoose.model('Thermos', ThermosSchema);
