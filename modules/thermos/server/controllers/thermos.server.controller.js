'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Thermo = mongoose.model('Thermos'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Thermo
 */
exports.create = function (req, res) {
  var thermo = new Thermo(req.body);
  thermo.user = req.user;

  thermo.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(thermo);
    }
  });
};

/**
 * Show the current Thermo
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var thermo = req.thermo ? req.thermo.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  thermo.isCurrentUserOwner = !!(req.user && thermo.user && thermo.user._id.toString() === req.user._id.toString());

  res.json(thermo);
};

/**
 * Update a Thermo
 */
exports.update = function (req, res) {
  var thermo = req.thermo;

  thermo = _.extend(thermo, req.body);

  thermo.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(thermo);
    }
  });
};

/**
 * Delete an Thermo
 */
exports.delete = function (req, res) {
  var thermo = req.thermo;

  thermo.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(thermo);
    }
  });
};

/**
 * List of Thermos
 */
exports.list = function (req, res) {
  var currUser = req.user;
  var criteria = {
    'user': ''
  };

  if (currUser) {
    if (currUser.roles.indexOf('admin') < 0) {
      if (currUser.roles.indexOf('user') >= 0) {
        criteria = {
          'user': currUser._id
        };
      }
    } else {
      // List all thermos
      criteria = undefined;
    }
  }

  Thermo.find(criteria).sort('-created').populate('user', 'displayName').exec(function (err, thermometers) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(thermometers);
    }
  });
};

/**
 * Thermometer middleware
 */
exports.thermoById = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(422).send({
      message: 'Thermometer is invalid'
    });
  }

  Thermo.findById(id).populate('user', 'displayName').exec(function (err, thermo) {
    if (err) {
      return next(err);
    } else if (!thermo) {
      return res.status(404).send({
        message: 'No Thermo with that identifier has been found'
      });
    }
    req.thermometer = thermo;
    next();
  });
};
