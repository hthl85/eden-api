'use strict';

/**
 * Module dependencies
 */
var thermosPolicy = require('../policies/thermos.server.policy'),
  thermos = require('../controllers/thermos.server.controller');

module.exports = function(app) {
  // Thermos Routes
  app.route('/api/thermos').all(thermosPolicy.isAllowed)
    .get(thermos.list)
    .post(thermos.create);

  app.route('/api/thermos/:thermoId').all(thermosPolicy.isAllowed)
    .get(thermos.read)
    .put(thermos.update)
    .delete(thermos.delete);

  // Finish by binding the Thermor middleware
  app.param('thermoId', thermos.thermoById);
};
