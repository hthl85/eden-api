'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Device Id need to be unique
 */
var validateUniqueIdByOwner = function (property, done) {
  var userId = this.user;
  var deviceId = property;

  mongoose.models.Devices.count({
    user: userId,
    id: deviceId
  }, function (err, count) {
    if (err) {
      return done(err);
    }
    // If `count` is greater than zero, "invalid"
    return done(!count);
  });
};

/**
 * Strict format only allows letters, numbers, and [.-_]
 */
var validateStrictFormat = function (property) {
  var regex = /^[a-zA-Z0-9]+([-_\.][a-zA-Z0-9]+)*[a-zA-Z0-9]*$/;
  return regex.test(property);
};

/**
 * Ensure the length of property within range
 */
var validateLength = function (property) {
  var regex = /^.{6,64}$/;
  return regex.test(property);
};

/**
 * Generate random secret token
 */
var generateSecret = function () {
  return function () {
    var randtoken = require('rand-token');
    return randtoken.generate(16);
  };
};

/**
 * Devices Schema
 */
var DevicesSchema = new Schema({
  id: {
    type: String,
    default: '',
    required: 'Please fill device Id',
    validate: [
      { validator: validateUniqueIdByOwner, msg: 'Device Id need to be unique' },
      { validator: validateStrictFormat, msg: 'Device Id is only allowed letter, number, and [.-_].' },
      { validator: validateLength, msg: 'Device Id must have length between 6 - 64' }
    ]
  },
  name: {
    type: String,
    default: '',
    required: 'Please fill device name',
    trim: true,
    validate: [
      { validator: validateLength, msg: 'Device Name must have length between 6 - 64' }
    ]
  },
  desc: {
    type: String,
    default: ''
  },
  subTopic: {
    type: String,
    default: '',
    required: 'Please enter device subscribe topic',
    validate: [
      { validator: validateStrictFormat, msg: 'Sub Topic is only allowed letter, number, and [.-_].' },
      { validator: validateLength, msg: 'Sub Topic must have length between 6 - 64' }
    ]
  },
  pubTopic: {
    type: String,
    default: '',
    required: 'Please enter device publish topic',
    validate: [
      { validator: validateStrictFormat, msg: 'Pub Topic is only allowed letter, number, and [.-_].' },
      { validator: validateLength, msg: 'Pub Topic must have length between 6 - 64' }
    ]
  },
  secret: {
    type: String,
    default: generateSecret(),
    required: 'Please enter device secret'
  },
  created: {
    type: Date,
    default: Date.now
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  }
});

mongoose.model('Devices', DevicesSchema);
