'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Device = mongoose.model('Devices'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  _ = require('lodash');

/**
 * Create a Device
 */
exports.create = function (req, res) {
  var device = new Device(req.body);
  device.user = req.user;

  device.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(device);
    }
  });
};

/**
 * Show the current Device
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var device = req.device ? req.device.toJSON() : {};

  // Add a custom field to the Article, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the Article model.
  device.isCurrentUserOwner = !!(req.user && device.user && device.user._id.toString() === req.user._id.toString());

  res.json(device);
};

/**
 * Update a Device
 */
exports.update = function (req, res) {
  var device = req.device;

  device = _.extend(device, req.body);

  device.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(device);
    }
  });
};

/**
 * Delete an Device
 */
exports.delete = function (req, res) {
  var device = req.device;

  device.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(device);
    }
  });
};

/**
 * List of Devices
 */
exports.list = function (req, res) {
  var currUser = req.user;
  var criteria = {
    'user': ''
  };

  if (currUser) {
    if (currUser.roles.indexOf('admin') < 0) {
      if (currUser.roles.indexOf('user') >= 0) {
        criteria = {
          'user': currUser._id
        };
      }
    } else {
      // List all devices
      criteria = undefined;
    }
  }

  Device.find(criteria).sort('-created').populate('user', 'displayName').exec(function (err, devices) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(devices);
    }
  });
};

exports.listRange = function (req, res) {
  var limit = parseInt(req.limit, 10);
  var pivot = req.pivot;
  var currUser = req.user;
  var criteria = [];
  var totalCriteria = {};

  if (currUser) {
    if (currUser.roles.indexOf('admin') < 0) {
      if (currUser.roles.indexOf('user') >= 0) {
        totalCriteria = { user: currUser._id };
        criteria.push(totalCriteria);
      } else {
        return res.status(422).send({
          message: 'Permission deny. Please contact admin to solve the issue.'
        });
      }
    }
  }

  if (pivot) {
    if (!mongoose.Types.ObjectId.isValid(pivot)) {
      return res.status(422).send({
        message: 'Last device id value is invalid'
      });
    } else {
      if (limit >= 0) {
        criteria.push({ _id: { $gt: pivot } });
      } else {
        criteria.push({ _id: { $lt: pivot } });
      }
    }
  }

  var query;

  if (limit > 0) {
    query = Device.find({ $and: criteria }).limit(Math.abs(limit)).populate('user', 'displayName');
  } else {
    query = Device.find({ $and: criteria }).sort({ _id: -1 }).limit(Math.abs(limit)).populate('user', 'displayName');
  }

  query.exec(function (err, devices) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      if (limit < 0) {
        devices.reverse();
      }
      Device.count(totalCriteria).exec(function (e, count) {
        if (e) {
          return res.status(422).send({
            message: errorHandler.getErrorMessage(err)
          });
        } else {
          res.json({
            data: devices,
            total: count
          });
        }
      });
    }
  });
};

/**
 * Device middleware
 */
exports.deviceByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(422).send({
      message: 'Device is invalid'
    });
  }

  Device.findById(id).populate('user', 'displayName').exec(function (err, device) {
    if (err) {
      return next(err);
    } else if (!device) {
      return res.status(404).send({
        message: 'No Device with that identifier has been found'
      });
    }
    req.device = device;
    next();
  });
};

/**
 * Device middleware
 */
exports.deviceByInputID = function (req, res, next, id) {

  // TODO: Check valid id
  var currUser = req.user;
  var criteria = {
    'id': id
  };

  if (currUser) {
    criteria.user = currUser._id;
  }

  Device.findOne(criteria).populate('user', 'displayName').exec(function (err, device) {
    if (err) {
      return next(err);
    } else if (!device) {
      return res.status(404).send({
        message: 'No Device with that identifier has been found'
      });
    }
    req.device = device;
    next();
  });
};

/**
 * Device middleware
 */
exports.setLimit = function (req, res, next, limit) {
  console.log('limit is ', limit);
  var tmp = parseInt(limit, 10);
  if (isNaN(tmp)) {
    return res.status(422).send({
      message: 'Limit value is invalid'
    });
  }
  req.limit = limit;
  next();
};

/**
 * Device middleware
 */
exports.setPivot = function (req, res, next, pivot) {
  console.log('pivot index is ', pivot);
  req.pivot = pivot;
  next();
};
