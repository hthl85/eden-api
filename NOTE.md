# Developing note

## Remove default client 

- Comment out module config `initModulesClientRoutes` in `express.js` since I planned to build client using `Angular2`

## Set up broker

- ### Install `mosquitto`
```sh
brew install mosquitto
```

- ### Install `pkg-config`
```sh
brew install pkgconfig
```

- ### Install `zeromq`
```sh
brew install zeromq
```

- ### Install `zmp`
```sh
npm install zmq -g
```

## Add dependencies

- ### Add `console timestamp` for debugging
```sh
npm install console-timestamp --save-dev
```

- ### Add `mosca`
```sh
npm install mosca --save-dev
```

- ### Add `mqtt`
```sh
npm install mqtt --save-dev
```

- ### Add `cors`
```sh
npm install cors --save
```

## Test broker
```sh
mosquitto_sub -h 127.0.0.1 -p 4883 -t 'test/topic'
mosquitto_pub -h 127.0.0.1 -p 4883 -t 'test/topic' -m 'helloWorld'
```

## Implement broker

- Check `broker.js` for mqtt broker implementation.
- Then attach broker to http server in `socket.io.js`.

## Yo generate

- Generate `express model`
```sh
yo meanjs:express-model <model-name>
```

- Generate `express controller`
```sh
yo meanjs:express-controller <controller-name>
```

- Generate `express route`
```
yo meanjs:express-route <route-name>
```

## Add `isAuth` API

- In the client, the `guard` that implemented from `CanActivate` class
need to known if current user is authenticated. I added `isAuth` api for this purpose.
Check this [link](http://stackoverflow.com/questions/14188834/documentation-for-ensureauthentication-isauthenticated-passports-functions) for more information.